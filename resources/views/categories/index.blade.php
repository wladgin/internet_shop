@extends('template')

@section('content')
    <div class="col-xs-12">
        <h1 class="my-4">{{$category['name']}}</h1>

       <div class="row">
           @include('products/small_view')
       </div>
    </div>

@endsection