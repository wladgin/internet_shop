<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['id', 'name', 'sort'];

    public static function getList()
    {
        $categories = self::all();

        $list = [];

        foreach ($categories as $category) {
            $list[$category['id']] = $category['name'];
        }

        return $list;
    }
}
