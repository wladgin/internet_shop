@extends('template')

@section('content')
    <div class="col-md-12">

        <form action="/order" method="post" class="form-horizontal">
            {{csrf_field()}}
            <div>
                <h2>Ваши заказы:</h2>

                <ul>
                    @foreach($products as $product)
                        <li>
                            <strong>{{$product->title}}</strong>
                            x {{$cart[$product->id]['amount']}} : ${{$cart[$product->id]['amount'] * $product->price}}
                        </li>
                    @endforeach
                </ul>
            </div>

            @include('embed.errors')

            <div class="form-group">
                <label for="customer_name">Имя Фамилия Отчество:</label>
                <input type="text" name="customer_name" id="customer_name" class="form-control">
            </div>


            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" id="email" class="form-control">
            </div>

            <div class="form-group">
                <label for="phone">Телефон:</label>
                <input type="text" name="phone" id="phone" class="form-control">
            </div>

            <div class="form-group">
                <label for="feedback">Особые пожелания:</label>
                <textarea name="feedback" id="feedback" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-primary">Оформить заказ</button>
            </div>


        </form>

    </div>
@endsection

@section('carousel')
    <div class="container m-4">
        <h1 class="display-3">Оформление заказа:</h1>
    </div>
    <br>
@endsection