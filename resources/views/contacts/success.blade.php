@extends('template')

@section('content')
    <div class="col-xs-12 my-5 mx-5 mx-auto" align="center">
        <div class="alert alert-success my-4 mx-5" role="alert" >
            <h2 class=\"alert-heading\">Сообщение принято!</h2>
            <p>Наша команда ответит вам в ближайшее время =)</p>
        </div>
    </div>

    <script type="text/javascript">
        setTimeout(function () {
            location.href = '/';
        }, 5000/* 5 seconds*/);
    </script>

@endsection