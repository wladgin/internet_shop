@extends('admin.template')
@section('content')

    <div class="col-xs-12">


        <form action="/admin/categories/edit/{{$category['id']}}" method="post" class="form-horizontal">

            @include('embed.errors')
            <div class="container">
                <h1>Edit category</h1>
            </div>

            {{csrf_field()}}

            {{method_field('POST')}}

            <div class="form-group">

                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control" value="{{$category['name']}}">

            </div>
            <div class="form-group">

                <label for="sort">Sort:</label>
                <input type="text" name="sort" id="sort" class="form-control" value="{{$category['sort']}}">

            </div>
            <div class="form-group">
                <button class="btn btn-default">Update</button>
            </div>


        </form>
    </div>

@endsection
