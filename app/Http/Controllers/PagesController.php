<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function show(Page $page){

        return view('pages.show', compact('page'));

    }

    public function index(){
        $page = Page::all();
        return view('pages.main', compact('page'));
    }

    public function create(){
        return view('pages.create');
    }

    public function store(){
        $this->validate(request(),[
            'title' => 'required|min:4|unique:pages,title',
            'alias' => 'required|min:4|unique:pages,alias',
            'intro' => 'required|min:4',
            'content' => 'required|min:4'
        ]);
        Page::create(request(['title', 'alias', 'intro','content']));


        return redirect('/');
    }

    public function edit(Page $page){
        return view('pages.edit', compact('page'));
    }

    public function update(Page $page){
        $this->validate(request(),[
            'title' => 'required|min:4|unique:pages,title,' . $page->id,
            'alias' => 'required|min:4|unique:pages,alias,' . $page->id,
            'intro' => 'required|min:4',
            'content' => 'required|min:4'

        ]);

        $page->update(request(['title', 'alias', 'intro', 'content']));

        return redirect('/pages');
    }

    public function delete(Page $page){
        return view('pages.delete', compact('page'));
    }
}
