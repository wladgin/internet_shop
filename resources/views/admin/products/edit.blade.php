@extends('admin.template')

@section('content')
    <div class="col-md-12">

        <form action="/admin/products/edit/{{$product['id']}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            {{method_field('POST')}}

            <div class="form-group">

                <label for="title">Название товара:</label>
                <input type="text" value="{{$product['title']}}" name="title" id="title" class="form-control">

            </div>
            <div class="form-group">

                <label for="category_id">Категория:</label>
                <select name="category_id" id="category_id" class="form-control">
                    <option value="">Выберите категорию</option>
                    @foreach($categoriesList as $id => $name)
                        <option value="{{$id}}"{{$product['category_id'] == $id ? ' selected="true"' : ''}}>{{$name}}</option>
                    @endforeach
                </select>

            </div>
            <div class="form-group">

                <label for="thumbnail"> Thumbnail </label>
                <input type="file" class="form-control" multiple name="thumbnail[]" id="thumbnail">

            </div>
            <div class="form-group">

                <label for="alias">Alias:</label>
                <input type="text" value="{{$product['alias']}}" name="alias" id="alias" class="form-control">

            </div>
            <div class="form-group">

                <label for="price">Цена:</label>
                <input type="text" value="{{$product->price}}" name="price" id="price" class="form-control">

            </div>
            <div class="form-group">

                <label for="description">Описание товара:</label>
                <textarea name="description" id="description" class="form-control">{{$product->description}}</textarea>

            </div>

            <div class="form-group">
                <button class="btn btn-default">Update</button>
            </div>


        </form>
    </div>

@endsection

@section('carousel')
    <div class="container">
        <br><br><br>
        <h1 class="display-4">Редактировать товар:</h1>
    </div>
    <br>
@endsection