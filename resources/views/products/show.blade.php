@extends('template')

@section('content')

    <div class="col-md-12">
        <h2 align="center">{{ $product['title'] }}</h2>
        <h3>Описание:</h3>
        <p> {{ $product['description'] }} </p>
        <p> <h3>Цена:</h3><h6>{{ $product['price'] }} грн</h6></p>
        <p><a class="btn btn-primary" href="/cart/{{ $product['alias'] }}" role="button">Buy »</a></p>
    </div>


@endsection

@section('carousel')
    <br><br>
    <p style="text-align: center">
        <img class="card-img-top" src="/uploads/{{$product->thumbnails->first()['name']}}" alt="" style="width: 300px; height: 300px" >
    </p>
    {{--<div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="First slide">
            </div>

            <div class="carousel-item">
                <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>--}}
@endsection