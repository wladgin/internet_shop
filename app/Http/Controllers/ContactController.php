<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function show(){
        return view('contacts.show');
    }

    public function store(){

        $this->validate(request(), [
            'customer_name' => 'required|min:3',
            'email' => 'required|email',
            'massage' => 'required|min:5'
        ]);

        Contact::create(request(['customer_name', 'subject', 'email', 'massage']));

        return view('contacts.success');
    }
}
