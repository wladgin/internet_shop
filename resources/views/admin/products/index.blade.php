@extends('admin.template')

@section('content')

    <div class="col-xs-12">
        <table class="table table-hovered table-bordered">
            <thead>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Price</td>
                <td>Category</td>
                <td>#</td>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{ $product['id'] }}</td>
                <td>{{ $product['title'] }}</td>
                <td>{{ $product['price'] }}</td>
                <td>{{ $categoriesList[$product['category_id']] }}</td>
                <td>
                    <a href="/admin/products/edit/{{$product['id']}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                    <a href="/admin/products/delete/{{$product['id']}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection