@extends('template')

@section('content')
    <div class="col-xs-12  m-5" >
        <h2>Selected products:</h2>
        <table class="table">
            <tr>
                <th>Product</th>
                <th>Price</th>
                <th>Amount</th>
                <th>Total price</th>
                <th></th>
            </tr>
            @foreach($products as $product)

                <tr>
                    <td>{{$product->title}}</td>
                    <td>{{$product->price}}</td>
                    <td>
                        <a href="/cart/{{$product->alias}}/remove" class="btn btn-warning">-</a>
                        {{$cart[$product->id]['amount']}}
                        <a href="/cart/{{$product->alias}}" class="btn btn-success">+</a>
                    </td>
                    <td>{{$cart[$product->id]['totalPrice']}}</td>
                    <td><a href="/cart/{{$product->alias}}/destroy" class="btn btn-danger">X</a></td>
                </tr>
            @endforeach
        </table>

        <div class="form-group">
            <a href="/order" class="btn btn-primary">Оформить заказ</a>
        </div>
    </div>

@endsection