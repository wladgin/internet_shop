@extends('sessions.template')
@section('content')


    <div class="col-md-12" >
        <h3 align="center">{{ $page['intro'] }}</h3>
        <br>
        <p> {{ $page['content'] }} </p>
        <a href="http://sipnotes.loc/pages" class="href">Вернуться назад</a>
    </div>

@endsection

@section('carousel')
    <div class="container">
        <br><br><br><br>
        <h1 class="display-4" align="center">{{ $page['title'] }}</h1>
    </div>
@endsection