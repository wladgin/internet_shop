<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function homepage(){
        $products = Product::with('thumbnails')->get();
        return view('main', compact('products'));
    }
}
