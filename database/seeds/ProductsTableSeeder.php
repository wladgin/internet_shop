<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => "Смартфон Apple iPhone 8 64GB Space Gray",
                'alias' => "apple_iphone-8-64gb-space-gray",
                'price' => 26499,
                'description' => "Apple iPhone 8 смартфон от 2017 года. Обладает процессором Quad-core, Mistral с частотой 2390 МГц. Размер экрана 4.7 дюймов. Основная камера 12 Мпикс. Аккумулятор имеет емкость 1821 мАч. Операционная система iOS 11.",
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'title' => "Смартфон Apple iPhone 8 Plus 64GB Gold",
                'alias' => "apple_iphone-8-plus-64gb-gold",
                'price' => 30999,
                'description' => "Apple iPhone 8 Plus смартфон от 2017 года. Обладает процессором Quad-core, Mistral с частотой 2390 МГц. Размер экрана 5.5 дюймов. Основная камера 12 Мпикс. Аккумулятор имеет емкость 2691 мАч. Операционная система iOS 11.",
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'title' => "Смартфон APPLE iPhone X 64GB Space Gray",
                'alias' => "apple_iphone-x-64gb-space-gray",
                'price' => 26499,
                'description' => "Apple iPhone X смартфон от 2017 года. Обладает процессором Quad-core, Mistral с частотой 2390 МГц. Размер экрана 5.8 дюймов. Основная камера 12 Мпикс. Аккумулятор имеет емкость 2716 мАч. Операционная система iOS 11",
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'title' => "Смартфон APPLE iPhone 7 32Gb Gold",
                'alias' => "apple_iphone-7-32gb-gold",
                'price' => 19499,
                'description' => "Apple iPhone 7 смартфон от 2016 года. Обладает процессором Quad-core, A10 Fusion с частотой 2340 МГц. Размер экрана 4.7 дюймов. Основная камера 12 Мпикс. Аккумулятор имеет емкость 1960 мАч. Операционная система iOS 10.0.1.",
                'created_at' => date("Y-m-d H:i:s")
            ]
        ]);
    }
}
