@extends('template')
@section('content')

    <div class="col-md-12">


        <form action="/pages/{{$page->alias}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            {{method_field('PATCH')}}

            <div class="form-group">

                <label for="title">Название статьи:</label>
                <input type="text" value="{{$page->title}}" name="title" id="title" class="form-control">

            </div>
            <div class="form-group">

                <label for="alias">Alias:</label>
                <input type="text" value="{{$page->alias}}" name="alias" id="alias" class="form-control">

            </div>
            <div class="form-group">

                <label for="intro">Вступление:</label>
                <input type="text" value="{{$page->intro}}" name="intro" id="intro" class="form-control">

            </div>
            <div class="form-group">

                <label for="content">Статья:</label>
                <textarea name="content" id="content" class="form-control">{{$page->content}}</textarea>

            </div>

            <div class="form-group">
                <button class="btn btn-default">Save</button>
            </div>


        </form>
    </div>

@endsection

@section('carousel')
    <div class="container">
        <br><br><br>
        <h1 class="display-4">Редактировать статью:</h1>
    </div>
    <br>
@endsection