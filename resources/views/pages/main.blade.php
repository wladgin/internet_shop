@extends('template')

@section('content')

    @foreach($page as $pages)

        <div class="col-lg-4 col-md-4">
            <div class="card h-50">
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="/pages/{{ $pages['alias'] }}">{{ $pages['title'] }}</a>
                    </h4>
                    <h5> {{ $pages['intro'] }}</h5>
                    <p><a class="btn btn-primary" href="/pages/{{ $pages['alias'] }}" role="button">Details »</a></p>
                    {{--@if(Auth::check())
                        <p><a class="btn btn-success" href="/pages/{{ $pages['alias'] }}/edit" role="button">Edit »</a></p>
                        <p><a class="btn btn-danger" href="/pages/{{ $pages['alias'] }}/delete" role="button">Delete »</a></p>
                    @endif--}}
                </div>
            </div>
        </div>

    @endforeach

@endsection

@section('carousel')
    <div class="container">
        <br><br><br>
        <h1 class="display-4" align="center">Статьи про технику:</h1>
    </div>
    <br>
@endsection