<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
 * [GET] /posts - all entries
 * [GET] /posts/{post} - entry
 * [POST] /posts - create entry
 * [PUT/PATCH] /posts/{post} - update entry
 * [DELETE] /posts/{post} - delete entry
 */

Route::get('/','HomeController@homepage');
Route::get('/products/{product}/delete', 'ProductsController@delete');
Route::get('/pages/{page}/delete', 'PagesController@delete');

/*Route::get('/products/create', 'ProductsController@create');
Route::get('/products/{product}', 'ProductsController@show');
Route::post('/products', 'ProductsController@store');
Route::get('/products/{product}/edit', 'ProductsController@edit');
Route::patch('/products/{product}', 'ProductsController@update');
Route::delete('/products/{product}', 'ProductsController@destroy');

Route::get('/pages', 'PagesController@index');
Route::get('/pages/create', 'PagesController@create');
Route::get('/pages/{page}', 'PagesController@show');
Route::post('/pages', 'PagesController@store');
Route::get('/pages/{page}/edit', 'PagesController@edit');
Route::post('/pages/{page}', 'PagesController@update');*/

Route::resources([
    'products' => 'ProductsController',
    'pages' =>'PagesController',
    'categories' => 'CategoriesController',
]);

Route::get('/login', 'SessionsController@create')->name('login');
Route::post('/sessions', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::get('/cart', 'CartController@index');
Route::get('/cart/{product}', 'CartController@store');
Route::get('/cart/{product}/remove', 'CartController@remove');
Route::get('/cart/{product}/destroy', 'CartController@destroy');

Route::get('/order', 'OrdersController@create');
Route::post('/order', 'OrdersController@store');

Route::get('/admin', 'admin\IndexController@index');

Route::get('/admin/categories/index', 'admin\CategoriesController@index');
Route::get('/admin/categories/create', 'admin\CategoriesController@create');
Route::post('/admin/categories/create', 'admin\CategoriesController@store');
Route::get('/admin/categories/edit/{id}', 'admin\CategoriesController@edit');
Route::post('/admin/categories/edit/{id}', 'admin\CategoriesController@update');
Route::get('/admin/categories/delete/{id}', 'admin\CategoriesController@destroy');

Route::get('/admin/products/index', 'admin\ProductsController@index');
Route::get('/admin/products/create', 'admin\ProductsController@create');
Route::post('/admin/products/create', 'admin\ProductsController@store');
Route::get('/admin/products/edit/{id}', 'admin\ProductsController@edit');
Route::post('/admin/products/edit/{id}', 'admin\ProductsController@update');
Route::get('/admin/products/delete/{id}', 'admin\ProductsController@destroy');

Route::get('/contact', 'ContactController@show');
Route::post('/contact', 'ContactController@store');

//Route::get('/admin/products', 'admin\ProductsController@create');
//Route::post('/admin/products', 'admin\ProductsController@store');
