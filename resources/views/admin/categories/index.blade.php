@extends('admin.template')

@section('content')

    <div class="col-xs-12">
        <table class="table table-hovered table-bordered">
            <thead>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Sort</td>
                <td>#</td>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
            <tr>
                <td>{{ $category['id'] }}</td>
                <td>{{ $category['name'] }}</td>
                <td>{{ $category['sort'] }}</td>
                <td>
                    <a href="/admin/categories/edit/{{$category['id']}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                    <a href="/admin/categories/delete/{{$category['id']}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection