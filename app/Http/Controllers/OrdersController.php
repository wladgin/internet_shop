<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class OrdersController extends Controller
{
    public function create(){
        $cart = json_decode(request()->cookie('cart'), true);
        if(count($cart) < 1){
            return redirect('/');
        }
        $products = [];
        foreach ($cart as $productId => $amount){
            $products[] = Product::find($productId);
        }
        return view('orders.create', compact('products', 'cart'));
    }

    public function store(){
        $cart = json_decode(request()->cookie('cart'), true);
        if(count($cart) < 1){
            return redirect('/');
        }
        $this->validate(request(), [
            'customer_name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:10'
        ]);

        $order = Order::create(request(['customer_name', 'email', 'phone', 'feedback']));

        foreach ($cart as $productId => $productAmount){
            $order->products()->attach($productId, ['amount' => $productAmount['amount']]);
        }
        return redirect('/')->withCookie('cart', json_encode([]));
    }
}
