@extends('template')
@section('content')

    <div class="col-xs-12">


        <form action="/products" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            <div class="form-group">

                <label for="title">Название товара:</label>
                <input type="text" name="title" id="title" class="form-control">

            </div>
            <div class="form-group">

                <label for="alias">Alias:</label>
                <input type="text" name="alias" id="alias" class="form-control">

            </div>
            <div class="form-group">

                <label for="price">Цена:</label>
                <input type="text" name="price" id="price" class="form-control">

            </div>
            <div class="form-group">

                <label for="description">Описание товара:</label>
                <textarea name="description" id="description" class="form-control"></textarea>

            </div>

            <div class="form-group">
                <button class="btn btn-default">Save</button>
            </div>


        </form>
    </div>

@endsection

@section('carousel')
    <div class="container">
        <br><br><br>
        <h1 class="display-4">Создать новый товар:</h1>
    </div>
    <br>
@endsection