<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Product;
use App\Thumbnail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $products = Product::all();
        return view('admin.products.index', [
            'products' => $products,
            'categoriesList' => Category::getList(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create', [
            'categoriesList' => Category::getList(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'title' => 'required|min:4|unique:products,title',
            'alias' => 'required|min:4|unique:products,alias',
            'price' => 'required',
            'category_id' => 'required|integer',
            'description' => 'required|min:4'
        ]);

        $product = Product::create(request(['title', 'alias', 'price','description', 'category_id']));

        if(count(request()->files->get('thumbnail'))){
            foreach (request()->files->get('thumbnail') as $file){

                $file = $file->move(public_path().'/uploads/', time().'_'.$file->getClientOriginalName());

                $thumbnail = Thumbnail::create([
                    'name' => basename($file->getRealPath()),
                    'size' => basename($file->getSize())
                ]);

                $product->thumbnails()->attach($thumbnail->id);


            }
        }


        return redirect('/admin/products/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::select()->where('id', '=', $id)->first();

        return view('admin.products.edit', [
            'product' => $product,
            'categoriesList' => Category::getList(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id){
        $product = Product::select()->where('id', '=', $id)->first();

        $this->validate(request(),[
            'title' => 'required|min:4|unique:products,title,' . \request('id'),
            'alias' => 'required|min:4|unique:products,alias,' . \request('id'),
            'price' => 'required|numeric',
            'category_id' => 'required|integer',
            'description' => 'required|min:4'
        ]);

        if(count(request()->files->get('thumbnail'))){
            foreach (request()->files->get('thumbnail') as $file){

                $file = $file->move(public_path().'/uploads/', time().'_'.$file->getClientOriginalName());

                $thumbnail = Thumbnail::create([
                    'name' => basename($file->getRealPath()),
                    'size' => basename($file->getSize())
                ]);

                $product->thumbnails()->attach($thumbnail->id);


            }
        }

        $product->update(request(['title', 'alias', 'price', 'description', 'category_id']));

        return redirect('/admin/products/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::select()->where('id', '=', $id)->first();

        $product->delete();

        return redirect('/admin/products/index');
    }
}
