@foreach($products as $product)

    <div class="col-md-4">
        <div class="card" style="margin-bottom: 15px;">
            <a href="/products/{{ $product['alias'] }}"><img class="card-img-top" src="/uploads/{{$product->thumbnails->first()['name']}}" alt=""></a>
            <div class="card-body">
                <h4 class="card-title">
                    <a href="/products/{{ $product['alias'] }}">{{ $product['title'] }}</a>
                </h4>
                <h5> {{ $product['price'] }} ₴</h5>
                <p class="card-text">{{ mb_strimwidth($product['description'], 0, 300, "...") }}</p>
                <p><a class="btn" href="/products/{{ $product['alias'] }}" role="button">Details »</a></p>
                <p><a class="btn btn-primary" href="/cart/{{ $product['alias'] }}" role="button">Buy »</a></p>
            </div>
        </div>
    </div>

@endforeach