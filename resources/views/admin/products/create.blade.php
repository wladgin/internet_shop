@extends('admin.template')
@section('content')

    <div class="col-md-6">


        <form action="/admin/products/create" method="post" class="form-horizontal" enctype="multipart/form-data">

            @include('embed.errors')
            <div class="container">
                <h1>Создать новый товар:</h1>
            </div>

            {{csrf_field()}}

            <div class="form-group">

                <label for="title">Название товара:</label>
                <input type="text" name="title" id="title" class="form-control">

            </div>
            <div class="form-group">

                <label for="category_id">Категория:</label>
                <select name="category_id" id="category_id" class="form-control">
                    <option value="">Выберите категорию</option>
                    @foreach($categoriesList as $id => $name)
                        <option value="{{$id}}">{{$name}}</option>
                    @endforeach
                </select>

            </div>
            <div class="form-group">

                <label for="thumbnail"> Thumbnail </label>
                <input type="file" class="form-control" multiple name="thumbnail[]" id="thumbnail">

            </div>
            <div class="form-group">

                <label for="alias">Alias:</label>
                <input type="text" name="alias" id="alias" class="form-control">

            </div>
            <div class="form-group">

                <label for="price">Цена:</label>
                <input type="text" name="price" id="price" class="form-control">

            </div>
            <div class="form-group">

                <label for="description">Описание товара:</label>
                <textarea name="description" id="description" class="form-control"></textarea>

            </div>

            <div class="form-group">
                <button class="btn btn-default">Save</button>
            </div>


        </form>
    </div>

@endsection
