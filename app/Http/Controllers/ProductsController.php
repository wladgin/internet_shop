<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth')->except('show');
    }

    public function show(Product $product){

        return view('products.show', compact('product'));
    }

    public function create(){
        return view('products.create');
    }

    public function store(){
        /*$product = new Product();
        $product->title = request('title');
        $product->alias = request('alias');
        $product->price = request('price');
        $product->description = request('description');
        $product->save();*/

        $this->validate(request(),[
            'title' => 'required|min:4|unique:products,title',
            'alias' => 'required|min:4|unique:products,alias',
            'price' => 'required',
            'description' => 'required|min:4|max:255'
        ]);

        Product::create(request(['title', 'alias', 'price','description']));


        return redirect('/');

    }

    public function edit(Product $product){

        return view('products.edit', compact('product'));
    }

    public function update(Product $product){
        $this->validate(request(),[
            'title' => 'required|min:4|unique:products,title,' . $product->id,
            'alias' => 'required|min:4|unique:products,alias,' . $product->id,
            'price' => 'required|numeric',
            'description' => 'required|min:4|max:255'
        ]);

        $product->update(request(['title', 'alias', 'price', 'description']));

        return redirect('/');
    }

    public function delete(Product $product){

        return view('products.delete', compact('product'));
    }

    public function destroy(Product $product){
        $product->delete();
        return redirect('/');
    }
}
