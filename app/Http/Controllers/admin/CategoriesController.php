<?php

namespace App\Http\Controllers\admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index(){
        $categories = Category::all();
        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request){
        $this->validate(request(),[
            'name' => 'required|min:4|unique:categories,name',
            'sort' => 'integer',
        ]);

        Category::create(request(['name', 'sort']));


        return redirect('/admin/categories/index');

    }

    public function edit($id){
        $category = Category::select()->where('id', '=', $id)->first();

        return view('admin.categories.edit', [
            'category' => $category,
        ]);
    }

    public function update($id){
        $product = Category::select()->where('id', '=', $id)->first();

        $this->validate(request(),[
            'name' => 'required|min:4|unique:categories,name,' . \request('id'),
            'sort' => 'integer',
        ]);

        $product->update(request(['name', 'sort']));

        return redirect('/admin/categories/index');
    }

    public function destroy($id)
    {
        $product = Category::select()->where('id', '=', $id)->first();

        $product->delete();

        return redirect('/admin/categories/index');
    }
}
