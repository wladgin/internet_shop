@extends('sessions.template')
@section('content')
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">

        <title>Signin Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Custom styles for this template -->

    </head>

    <body class="text-center">
    <form class="form-signin" style="margin-left:20%" method="post" action="/register">

        {{csrf_field()}}

        @include('embed.errors')

        <img class="mb-4" src="/images/logo.jpg" alt="" width="300" height="132">
        <h1 class="h3 mb-3 font-weight-normal">Registration</h1>
        <div>
            <label for="name" class="sr-only">Full name:</label>
            <input type="name" id="name" name="name" class="form-control" placeholder="Your full name" required autofocus>
        </div>

        <div>
            <label for="inputEmail" class="sr-only">Email address:</label>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
        </div>

        <div>
            <label for="inputPassword" class="sr-only">Password:</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        </div>

        <div>
            <label for="password_confirmation" class="sr-only">Password conformation:</label>
            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Password conformation" required>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        <br><br><br><br><br><br><br><br><br><br>

    </form>
    </body>


@endsection

@section('carousel')
    <div class="container">
        <br><br><br>
    </div>
    <br>
@endsection