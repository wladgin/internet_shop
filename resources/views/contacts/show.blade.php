@extends('template')

@section('content')
    <div class="col-xs-12">
        <div class="contact  mx-3">
            <p><span>
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAARbSURBVGhD7ZpbqFVFGICPXbWbJaQV1UsXhShIIwxUVMiEVLqISCDqg6X1lJmggVA+ZIWaUGiBYQ9RmtqFoKSUsAQhCC9UppJlUKhoZmaFmX3fxNB5WLe996y9j+AHH5yZM2dm1mVm/pl1us6SzTX4KL6FO/AXPI1/4c/4Gb6I9+L52OO4Bd/Fv9GOV/EgPoUXYce5ElfiKbRzf+KH+AjegT6h8/BivBlH4yL8BuMF/YTTsWPcivvQzpzA5/AKrMpd+CnGC3oNL8S2MhZ/QzuwCa/DZnkAj6J1fY59sS3chr+jDa/AFIN2EO5B69yA52KtOCb2ow2+bEZCrsIf0bqd2WrlDbShjeggTs0QjE97hBl1MBj/weM4wIyamINeyFbsZUZqPkIbeDqkqnMpXvDfj5XojT+gbTkRJOUG9Gk4U11mRglDcQ3+inZId+EzeDmWMRP9Gwd+UuaiFb8dUvk42yxBLzpewBF0nYnpAzgci+iPLrJGClUuvDIfoJ2YElL5LEPL2fH5GMeS77pPKb6eDmgHdhHGZZadEFKJ+B6t1DAjj5EYO+mqnYUX5LRtuZ1YtF4sRsvNC6lExFfFmCmP+NSeCKl8nLa/RsuONyOHx9Eyr4ZUAgwZrNBpNw9XdwPGk1glxIhTbNGiOhkt49qVBKfDsgsxyrXMdyFVzt1oeZ9iHg+hZV4PqUT8gVaat5obuvh7N09VcABbfl1IZfMYWsYJJBnuGazUO5+H06plbgypYl5Ayy4MqWz8nWUWhFQijK2sdExIZbMcLbMqpPIxODRsdwK53Ywc3kPrmxhSiTAatdInQyqb6zEGfHlTpuvKF2iZssU1btoGhlQipqKVepeKeBDjvn0LuoV1IXSL66tyGP2d4UrR7OZNsZxPLune5Fq0YmOnso3UPRj3FVmux35YhHt+yxZNBk3zFVq5K3gZfXAauga4fTX4Mwbz6VThfbSth0MqMc+ilb8SUvXh0/IczFfUiSE5npp4Ib7njewvGsWnYDvJQ/jubEMbmRRS9fAl2kZZpN0Ss9BGDLHrwInC+g+h46w2LsF4nlu0mDXLJ2jdRSt+Mp5HG1sbUunwxlivcd3VZtSNM4k7QLeiTgCpWI1eSOrzskKWoo2+E1Kt4w1xunXabeX4tWGMmeIJySgzWiSODRfNtmMAaePbsZV4KO5N/F6S9MSkKn4C2I12otlQwrjtW7QOp/aOcR/Gu9nM54B4wFB2otIWPkY74yeGRjBUP4b+rWF+x7kJnY7d8TUy8ONhXdmOsq3MRju1F4vOviKG+Jb3sKJsb9JWfL/dd9i5l8wowHXC82DL3m9GT6P7KzbOjAzOQb85ehFvmtFT8Z8F7KSzWFa8FE/1/f7Ro16pLNyT29nN2H0DNgzjzq+2z2op8Vt7PMqJp4QGmvGQL+mBW93cid59Oz4DfTr+7EFfxxe+RvFMy85H/cbiGfEZSTyh9ATSr8JnLL5GjomiM+Oz/E9X17/6sy9A5nhpzgAAAABJRU5ErkJggg==">
                </span> <b>Местоположение:</b>  проспект Шевченка, 4, Одеса, Одеська область, 65000
            </p>
            <p><span>
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPASURBVGhD7ZlXiFRJFIbHsEZWMWJasygm0BdFDIv6IoLgIuKbIIqIiuCDIiK+6YugKJhF0TUgrOlhXTHgqogPigETRswR45p11+9rpqCZvd3Ts9vdUwPzw4feuqfvPTVVdc6puiXVquKqUfpvlZTOT4Kr8DechT5QpfQj7IR/ynAfmkCVUCvwr6/jL2Ac1IOjpW0bIXp1gdugw5egMwR57y/w3mgbYlU7uAU6egySptB08P4DiHKKNYILoJOHoT4kyQAQ9RRbAzpnZxrbkEXpU2yUDTHpIxhicw2vs8CO3AUjXDT6BjpmdMpFNeE4+JvVNsSisMi7pa5yU3f4AI7kcBti0G6wIxNSV7lrDvi7A6mrCBTCakUj0WDwd+dTVxGoE7hO3kJFFu9ysCNrU1eRyOmhU9NSV+WrK3yCr9DLhlj0C9iRG/CDDeVoF2gfVdRShtRzoHMzbMiiYaDda2hpQ2wyU+vgY8i0VuzwadBurg2x6gjo5MrU1b/VBswf2oy1IVb1gPdgohthQ4Jmgh15Dj/ZEKtmg47egaQpZhW8D7Q5BQ0gStWCUEtttSFBVsmh9N8L/iZKmSdegY46QknqAE9Bmw0Q7WmLW1ozvklvpA0J6g+hwysg2s4sAJ18Bul7+HQNgneg3RIbYpR/4d9AJ10TzSFJjpgli3ZLIcqRMXKFhW2UypQsTaiGbu0sXaLsTAu4DjrpAUWm3eTP8Aa02wJ1IJusvIdCw9RVkWTyuwc6acitDUkaAB7uaXcIkg40BsKfoI1o7xFt0UbRLW4IueaYTPnDgwwTqnZXoDeo9rANrBy8Z5C4WPp/+QO0KYr6QQi5OyDT9PH41TUVHN4EYQ25iZsPoSrwaPYJeM+qegoUZXScGmH6/A6ZyhTX0irQTsxLJs/WUFZGxO0QbA9CRyi4nD6PwJd6zJrtcM9KeQ84muVJW7cSPtfA4a614KNjKRMOvs9AvjZaTeFXCKPj9sIIV1B5AH4ZfOFN6Av50hh4CD7bIFPwzji/T4IvdCGPh3zJk//94LMX2VBo1YV14AsNr77UrXE+NA98ruVP0eTi/Ay+2Mhjuf9/5PY6BJWifwEYAuHl5oXJ8F8ijznqBPgcw3ylqBmk5wWzdk/IVXY8RC4jo5GsUuXhX8jaX2A9tIVsshPhSNbEG82ncaPaMgh7Fj8uWXM5BctOOU86N4N2ljYWodHJPOB0CcFAXEuW/AthKoT67CVY4kctI5FF4zUIHUrHyjmfibUosjCcCIvBD7PmjIp83qhWtfKrkpLvQWkC5sBXTF0AAAAASUVORK5CYII=">
                </span> <b>Наш телефон:</b> +380982413050
            </p>
            <p><span>
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAJHSURBVGhD7dhLqA1xHMDx45FXJJFEkZJCWMhGSoosRCksRBEbWVgoRRaUjbJQSnkkyQZlpViIDSVuXUUWkkSyEHnm/fh+dUbndK5z58zj3Jnp/61P3XPuzJz5cec/c28tFAqFSte4ChiF2u8KOIZ/L96U0Ce0DFLGtqO6g8z3RclygJZB3mOFb5SgoTiF6NybBtF3bEWRG4OraDzvpkH24Vf964MYhKI1BffgOb7A8frXTYPYJnyFr89hGIrSXDyH53YfU9F21VoK12jfuwHvnAPdcryD53QNY2H9Lr9z8BS+/xDTMVBtwTd4LmfQ+FMS6z4yCT3wey+xEN3Ma/QA/Hyv3f319xqLfUMcjcvw+x+xGt3If/Wz8HP939iMvoo9iA2BG7rND+xAnnlNXoef9xbL8L86GiRqN6Ll+TAGI+um4QH8jGdwpWpXokFsPT7D7S9iBLJqAbwWPXYvJqO/Eg9ii/Ea7nMLE5C2lfgAj3kF3r3jlGoQm4nHcL9HmIGkeTJeex7rBHyOilvqQWwibsN9X2EROsml9BDc32tvLzotk0FsJC7B/b121iJOXlvn4X5fsAFJymwQc3k+Ao/xE7vQrvG4Cbf3UWgJkpbpIFE7Ef2s+1Ta14o2D15TbvMEs5CmXAaxNYj+IOCJ+qvBKmzEaURP1nfgI1DachvEZuMuouM28nHDm+lwZFGug5grko/eR+F9wQVhD/wdIstyH6RbhUGKVhikaIVBila1B1lXQifRMkiZ/R3kQgVsQygUCoW6Xa32B2Bmt8+eA9AkAAAAAElFTkSuQmCC">
                </span> <b>Почта:</b> office{{'@'}}sipnotes.ua
            </p>
            <br>
        </div>

        <form action="/contact" method="post" class="form-horizontal">
            {{csrf_field()}}
            <h3 align="center">По всем вопросам и предложениям вы можете написать письмо администратору:</h3>
            <br>

            @include('embed.errors')
            <div class="mx-3">
                <div class="form-group">
                    <label for="customer_name">Полное имя:</label>
                    <input type="text" name="customer_name" id="customer_name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="subject">Тема обращения:</label>
                    <input type="text" name="subject" id="subject" class="form-control">
                </div>


                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>


                <div class="form-group">
                    <label for="massage">Сообщение:</label>
                    <textarea name="massage" id="massage" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Отправить</button>
                </div>
            </div>



        </form>

    </div>
@endsection

@section('carousel')
    <div class="container mb-4">
        <br>
        <h1 class="display-4">Контакты:</h1>
    </div>
    <br>
@endsection