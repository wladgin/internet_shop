@extends('admin.template')
@section('content')

    <div class="col-xs-12">


        <form action="/admin/categories/create" method="post" class="form-horizontal">

            @include('embed.errors')
            <div class="container">
                <h1>Create new category</h1>
            </div>

            {{csrf_field()}}

            <div class="form-group">

                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control">

            </div>
            <div class="form-group">

                <label for="sort">Sort:</label>
                <input type="text" name="sort" id="sort" class="form-control">

            </div>
            <div class="form-group">
                <button class="btn btn-default">Add</button>
            </div>


        </form>
    </div>

@endsection
