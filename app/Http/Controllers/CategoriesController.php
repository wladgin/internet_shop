<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function show(Category $category)
    {
        $products = Product::select('*')->where('category_id', '=', $category['id'])->get();


        return view('categories.index', [
            'category' => $category,
            'products' => $products
        ]);
    }
}
