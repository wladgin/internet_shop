<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'customer_name' => "Tereshkov Vasya",
                'email' => "tereshkov@gmail.com",
                'phone' => "0981111111",
                'feedback'=> "Nice!",
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'customer_name' => "Serenkov Misha",
                'email' => "serenkov@gmail.com",
                'phone' => "0982222222",
                'feedback'=> "Good work!",
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'customer_name' => "Tsurkan Artem",
                'email' => "curkan@gmail.com",
                'phone' => "0983333333",
                'feedback'=> "Bad, very bad",
                'created_at' => date("Y-m-d H:i:s")
            ]
        ]);
    }
}
