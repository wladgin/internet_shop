@extends('template')

@section('content')
    <div class="col-md-12">


        <form action="/products/{{$product->alias}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            {{method_field('DELETE')}}

            <div class="form-group">

                <h3>Вы действительно хотите удалить {{$product->title}}?</h3>
            </div>


            <div class="form-group">
                <button class="btn btn-danger">Delete</button>
            </div>


        </form>
    </div>

@endsection

@section('carousel')
    <div class="container">
        <br><br><br>
        <h1 class="display-4">Удалить товар товар:</h1>
    </div>
    <br>
@endsection